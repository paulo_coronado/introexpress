let express=require('express');
let Contestador=require("./Contestador.js");


let app=express();

//1. Función normal
//2. Función anónima
//3. Función flecha
//4. Como un método


let miObjeto=new Contestador();

app.get('/',miObjeto.responder);

app.post('/insertar',miObjeto.insertar);



app.listen(5000);